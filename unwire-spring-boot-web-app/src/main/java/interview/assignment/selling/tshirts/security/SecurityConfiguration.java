package interview.assignment.selling.tshirts.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	/*
	 * To create our own security configuration extend WebSecurityConfigurerAdapter
	 */

	/*
	 * Create our own users using inMemoryAuthentication
	 */

	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {

		auth.inMemoryAuthentication().withUser("admin").password("admin").roles("USER", "ADMIN");

		auth.inMemoryAuthentication().withUser("user").password("user").roles("USER");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		/*
		 * Permit anybody for /login; however if somebody wants to go to the tshirts,
		 * orders pages we need to see if s/he has a role of USER. If s/he doesnt have a
		 * role of USER show them the default form login provided by spring boot.
		 */
		http.authorizeRequests().antMatchers("/login").permitAll().antMatchers("/", "/*tshirts*/**", "/*orders*/**")
				.access("hasRole('USER')").and().formLogin();

	}

	/*
	 * Create a login form
	 */
}
