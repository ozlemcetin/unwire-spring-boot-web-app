
package interview.assignment.selling.tshirts.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

/*
 * Use @SessionAttributes("userObj") to use the User session attribute which was put into the session by the LoginController.
 */

@Controller
@SessionAttributes("userObj")
public class LogoutController {

	@RequestMapping(value = { "/logout" }, method = RequestMethod.GET)
	public String logouTheLoggedInUser(HttpServletRequest request, HttpServletResponse response, ModelMap model) {

		// Get the authentication obj from spring security
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		//If the principal already authenticated, get her/him out.
		if(auth != null) {
			
			new SecurityContextLogoutHandler().logout(request, response, auth);
		
			model.remove("userObj");
		}

		/*
		 * Search for a view named welcome (aka welcome.jsp)
		 */
		return "redirect:/login";

	}

}
